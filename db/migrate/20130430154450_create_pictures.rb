class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :directory
      t.references :place

      t.timestamps
    end
    add_index :pictures, :place_id
  end
end
