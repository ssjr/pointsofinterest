class PlacesController < ApplicationController
  def index
    first = Picture.order("RANDOM()").first
    if first.nil?
      @index_image = ActionController::Base.helpers.asset_path("padrao.png")
    else
      @index_image = first.directory_url
    end
    @places = Place.all
  end

  def show
    @place = Place.find(params[:id])
  end
end
