ActiveAdmin.register Place do
  form do |f|
      f.inputs "Details" do
        f.input :name
        f.input :address
        f.input :description
      end
      f.actions
    end
end
