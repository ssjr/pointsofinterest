class Place < ActiveRecord::Base
  attr_accessible :address, :description, :latitude, :longitude, :name

  has_many :pictures
  validates :name, :address, :description, :presence => true

  geocoded_by :address

  after_validation :geocode, :if => lambda{ |obj| obj.address_changed? }
end
