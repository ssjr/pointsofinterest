class Picture < ActiveRecord::Base
  belongs_to :place
  attr_accessible :directory, :place_id

  validates :directory, :place, :presence => true

  mount_uploader :directory, PlacePictureUploader
end
